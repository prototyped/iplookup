// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the BSD License.
// See accompanying files LICENSE.

// +build !ignore

package iplookup

import (
	"testing"
)

func TestGeoIPLookup_Example(t *testing.T) {
	var params = map[string]string{
		"filepath":  "D:/Data/GeoLite2-City.mmdb",
		"lang": "zh-CN",
	}

	var lookup GeoIPLookup
	if err := lookup.Init(params); err != nil {
		t.Fatalf("Init: %v", err)
	}
	defer lookup.Shutdown()

	var addrs = []string{
		"8.8.8.8",
		"208.67.222.222",
		"182.148.200.29",
		"47.91.109.14",
		"143.191.195.98",
		"47.75.40.119",
	}

	for _, addr := range addrs {
		location, err := lookup.Lookup(addr)
		if err != nil {
			t.Errorf("Lookup: %v, %v", err, addr)
		}
		t.Logf("%s: [%v]", addr, location)
	}
}
