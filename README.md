# IPLookup
根据IP地址查询位置信息，提供查询GeoIP数据库和第三方Web Service两种方式

# 依赖的第三方库

* go get github.com/oschwald/geoip2-golang
* go get github.com/bitly/go-simplejson

# GeoIP数据库

[GeoLite2](https://dev.maxmind.com/geoip/geoip2/geolite2/)

# 第三方WebService API

* [百度地图](http://lbsyun.baidu.com/index.php?title=webapi/ip-api) 6000 req/day，仅国内IP
* [腾讯地图](https://lbs.qq.com/webservice_v1/guide-ip.html) 10,000 req/day
* [高德地图](https://lbs.amap.com/api/webservice/guide/api/ipconfig) 100,000 req/day，仅国内IP
* [IP138](http://user.ip138.com/ip/) 1000 req
* [ipdata](https://docs.ipdata.co/) 1500 req / day
* [ipapi](http://ip-api.com/docs/api:json) 150 req / min
* [ipify](https://geo.ipify.org/docs) 1000 req / month
