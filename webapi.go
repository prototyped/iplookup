// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the BSD License.
// See accompanying files LICENSE.

package iplookup

import (
	"bytes"
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	//"github.com/PuerkitoBio/goquery"
	"github.com/bitly/go-simplejson"
)

type WebAPILookup struct {
	client *http.Client
	params map[string]string
}

func (l *WebAPILookup) Init(params map[string]string) error {
	if params == nil {
		params = map[string]string{}
	}
	var client = &http.Client{Timeout: time.Second * 10}
	l.client = client
	l.params = params
	return nil
}

func (l *WebAPILookup) Shutdown() {
	if l.client != nil {
		l.client.CloseIdleConnections()
		l.client = nil
	}
}

func (l *WebAPILookup) Lookup(s string) (string, error) {
	var ip = net.ParseIP(s)
	if result := FilterSpecialIP(ip); result != "" {
		return result, nil
	}

	//location, err := l.requestQQLbs(s)
	//location, err := l.requestBaiduLbs(s)
	//location, err := l.requestAmap(s)
	//location, err := l.requestIP138(s)
	//location, err := l.requestIpify(s)
	//location, err := l.requestIpData(s)
	location, err := l.requestIpApi(s)

	if err != nil {
		return "", err
	}
	return location, nil
}

func HttpJsonRequest(client *http.Client, url string) (*simplejson.Json, error) {
	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP %d", resp.StatusCode)
	}
	defer resp.Body.Close()
	result, err := simplejson.NewFromReader(resp.Body)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// https://lbs.qq.com/webservice_v1/guide-ip.html
func (l *WebAPILookup) requestQQLbs(s string) (string, error) {
	var key = l.params["qqlbs-key"]
	var url = fmt.Sprintf("https://apis.map.qq.com/ws/location/v1/ip?key=%s&ip=%s&output=json", key, s)
	result, err := HttpJsonRequest(l.client, url)
	if err != nil {
		return "", err
	}
	var status = result.Get("status").MustInt()
	if status != 0 {
		var msg = result.Get("message").MustString()
		return "", fmt.Errorf(msg)
	}
	var info = result.Get("result").Get("ad_info").MustMap()
	var country = info["nation"]
	var province = info["province"]
	var city = info["city"]
	if country == "中国" || country == "China" {
		country = ""
	}
	var location = strings.TrimSpace(fmt.Sprintf("%s %s %s", country, province, city))
	return location, nil
}

// http://lbsyun.baidu.com/index.php?title=webapi/ip-api
func (l *WebAPILookup) requestBaiduLbs(s string) (string, error) {
	var ak = l.params["baidu-ak"]
	var url = fmt.Sprintf("http://api.map.baidu.com/location/ip?ip=%s&ak=%s", s, ak)
	result, err := HttpJsonRequest(l.client, url)
	if err != nil {
		return "", err
	}
	var status = result.Get("status").MustInt()
	if status != 0 {
		var msg = result.Get("message").MustString()
		return "", fmt.Errorf(msg)
	}
	var content = result.Get("content").MustMap()
	var address = fmt.Sprintf("%v", content["address"])
	return address, nil
}

// https://lbs.amap.com/api/webservice/guide/api/ipconfig
func (l *WebAPILookup) requestAmap(s string) (string, error) {
	var key = l.params["amap-key"]
	var url = fmt.Sprintf("https://restapi.amap.com/v3/ip?ip=%s&key=%s&output=json", s, key)
	result, err := HttpJsonRequest(l.client, url)
	if err != nil {
		return "", err
	}
	var status = result.Get("status").MustInt()
	if status != 1 {
		var msg = result.Get("info").MustString()
		return "", fmt.Errorf(msg)
	}
	var province = result.Get("province").MustString()
	var city = result.Get("city").MustString()
	var location = strings.TrimSpace(fmt.Sprintf("%s %s", province, city))
	return location, nil
}

// http://user.ip138.com/ip/
func (l *WebAPILookup) requestIP138(s string) (string, error) {
	var token = l.params["ip138-token"]
	var url = fmt.Sprintf("https://api.ip138.com/query/?ip=%s&token=%s", s, token)
	result, err := HttpJsonRequest(l.client, url)
	if err != nil {
		return "", err
	}
	var ret = result.Get("ret").MustString()
	if ret != "ok" {
		var msg = result.Get("msg").MustString()
		return "", fmt.Errorf(msg)
	}
	var data = result.Get("data").MustArray()
	var buf bytes.Buffer
	for i := 0; i < 3 && i < len(data); i++ {
		fmt.Fprintf(&buf, "%v ", data[i])
	}
	var location = strings.TrimSpace(buf.String())
	return location, nil
}

// https://geo.ipify.org/docs
func (l *WebAPILookup) requestIpify(s string) (string, error) {
	var apiKey = l.params["ipify-key"]
	var url = fmt.Sprintf("https://geo.ipify.org/api/v1?apiKey=%s&ipAddress=%s", apiKey, s)
	result, err := HttpJsonRequest(l.client, url)
	if err != nil {
		return "", err
	}
	var location = result.Get("location").MustMap()
	var text = fmt.Sprintf("%v %v %v", location["country"], location["region"], location["city"])
	text = strings.TrimSpace(text)
	return text, nil
}

// https://docs.ipdata.co/
func (l *WebAPILookup) requestIpData(s string)(string, error) {
	var key = l.params["ipdata-key"]
	var url = fmt.Sprintf("https://api.ipdata.co/%s?api-key=%s", s, key)
	result, err := HttpJsonRequest(l.client, url)
	if err != nil {
		return "", err
	}
	var country = result.Get("country_name").MustString()
	region, _ := result.Get("region").String()
	city, _ := result.Get("city").String()
	var location = strings.TrimSpace(fmt.Sprintf("%s %s %s", country, region, city))
	return location, nil
}

// http://ip-api.com/docs/api:jso
func (l *WebAPILookup) requestIpApi(s string) (string, error) {
	var lang = l.params["lang"]
	if lang == "" {
		lang = "zh-CN"
	}
	var url = fmt.Sprintf("http://ip-api.com/json/%s?lang=%s", s, lang)
	result, err := HttpJsonRequest(l.client, url)
	if err != nil {
		return "", err
	}
	var status = result.Get("status").MustString()
	if status != "success" {
		var msg = result.Get("message").MustString()
		return "", fmt.Errorf(msg)
	}
	var country = result.Get("country").MustString()
	var city = result.Get("city").MustString()
	var region = result.Get("regionName").MustString()
	var location = strings.TrimSpace(fmt.Sprintf("%s %s %s", country, region, city))
	return location, nil
}
