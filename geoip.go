// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the BSD License.
// See accompanying files LICENSE.

package iplookup

import (
	"fmt"
	"net"
	"strings"

	geoip2 "github.com/oschwald/geoip2-golang"
)

type GeoIPLookup struct {
	filepath string
	lang     string
	db       *geoip2.Reader
}

func (l *GeoIPLookup) Init(params map[string]string) error {
	var filepath = params["filepath"]
	var lang = params["lang"]
	if lang == "" {
		lang = "zh-CN"
	}
	db, err := geoip2.Open(filepath)
	if err != nil {
		return err
	}
	l.lang = lang
	l.filepath = filepath
	l.db = db
	return nil
}

func (l *GeoIPLookup) Shutdown() {
	if l.db != nil {
		l.db.Close()
		l.db = nil
	}
}

func (l *GeoIPLookup) Lookup(s string) (string, error) {
	var ip = net.ParseIP(s)
	if result := FilterSpecialIP(ip); result != "" {
		return result, nil
	}
	record, err := l.db.City(ip)
	if err != nil {
		return "", err
	}

	// format location text
	var city = record.City.Names[l.lang]
	var country = record.Country.Names[l.lang]
	var subdivision string
	for _, sd := range record.Subdivisions {
		subdivision = sd.Names[l.lang]
		break
	}
	if country == city {
		country = ""
	}
	var location string
	switch country {
	case "China", "中国":
		location = fmt.Sprintf("%s %s", subdivision, city)
	default:
		location = fmt.Sprintf("%s %s %s", country, subdivision, city)
	}
	location = strings.TrimSpace(location)
	return location, nil
}
