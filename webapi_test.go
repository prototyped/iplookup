// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the BSD License.
// See accompanying files LICENSE.

// +build !ignore

package iplookup

import (
	"testing"
)

func TestWebAPILookup_Example(t *testing.T) {
	var params = map[string]string{
		"baidu-ak": "",
		"qqlbs-key": "",
		"amap-key": "",
		"ip138-token": "",
		"ipify-key": "",
		"ipdata-key": "",
	}
	var lookup WebAPILookup
	lookup.Init(params)
	defer lookup.Shutdown()
	var addrs = []string{
		"8.8.8.8",
		"208.67.222.222",
		"182.148.200.29",
		"47.91.109.14",
		"143.191.195.98",
		"47.75.40.119",
	}

	for _, addr := range addrs {
		location, err := lookup.Lookup(addr)
		if err != nil {
			t.Errorf("Lookup failed: %v, %v", err, addr)
		} else {
			t.Logf("Succeed: %s => %s", addr, location)
		}
	}
}
