// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the BSD License.
// See accompanying files LICENSE.

package iplookup

import (
	"net"
)

type IPLookupWorker interface {
	//
	Init(map[string]string) error

	//
	Shutdown()

	//
	Lookup(string) string
}

func FilterSpecialIP(ip net.IP) string {
	if ip == nil {
		return "N/A"
	}
	if ip.IsLoopback() {
		return "Loopback"
	}
	if ip.IsMulticast() {
		return "Multicast"
	}
	if ip.IsUnspecified() {
		return "Unspecified"
	}

	return "" //
}