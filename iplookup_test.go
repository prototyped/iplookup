// Copyright © 2019 prototyped.cn. All rights reserved.
// Distributed under the terms and conditions of the BSD License.
// See accompanying files LICENSE.

// +build !ignore

package iplookup

import (
	"net"
	"testing"
)

type TestCase struct {
	input    string
	expected string
}

func TestFilterSpecialIP(t *testing.T) {
	var cases = []TestCase{
		{"", "N/A"},
		{"123", "N/A"},
		{"127.0.0.1", "Loopback"},
		{"182.148.200.29", ""},
	}

	//println(s,"IsLoopback", ip.IsLoopback())
	//println(s,"IsGlobalUnicast", ip.IsGlobalUnicast())
	//println(s,"IsInterfaceLocalMulticast", ip.IsInterfaceLocalMulticast())
	//println(s,"IsLinkLocalMulticast", ip.IsLinkLocalMulticast())
	//println(s,"IsLinkLocalUnicast", ip.IsLinkLocalUnicast())
	//println(s,"IsMulticast", ip.IsMulticast())
	//println(s,"IsUnspecified", ip.IsUnspecified())

	for _, item := range cases {
		var ip = net.ParseIP(item.input)
		var result = FilterSpecialIP(ip)
		if result != item.expected {
			t.Errorf("expect [%v], but got [%v], input: %v", item.expected, result, item.input)
		}
	}
}
